import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.image}>
            <Text style={styles.headerText}>Image</Text>
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.row}>
          <View style={styles.box1}>
            <Text style={styles.text}>TextInput1</Text>
            </View>
          </View>
          <View style={styles.row}>
          <View style={styles.box1}>
            <Text style={styles.text}>TextInput2</Text>
            </View>
          </View>

          <View style={styles.box2}>
            <Text style={styles.text2}>TouchbleOpacity</Text>
            </View>
          
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "gray",
    flex: 1
  },

  header: {
    backgroundColor: "gray",
    alignItems: "center",
    justifyContent: "center",
    flex: 1
    // width: 200,
    // height: 200,
    // borderRadius: 100
  },

  image: {
    backgroundColor: "white",
    alignItems: 'center',
    justifyContent: 'center',
    width: 200,
    height: 200,
    borderRadius: 100
  },

  headerText: {
    color: "black",
    fontSize: 30,
    fontWeight: "bold",
  },

  content: {
    backgroundColor: "gray",
    flex: 1,
    flexDirection: "column",
  },

  box1: {
    backgroundColor: "white",
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },

  box2: {
    backgroundColor: "black",
    flex: 1,
    margin: 55,
    alignItems: 'center',
    justifyContent: 'center'
  },

  row: {
    backgroundColor: "white",
    padding: 5,
    margin: 8,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  
  text: {
    color: "black",
    fontSize: 30,
    fontWeight: "bold"
  },
  
  text2: {
    color: "white",
    fontSize: 25
  }
});
export default App;