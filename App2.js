import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";

const instructions = Platform.select({
    ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
    android:
        "Double tap R on your keyboard to reload,\n" +
        "Shake or press menu button for dev menu"
});

class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.box1}>
                        <Text style={styles.text}>=</Text>
                    </View>
                    <View style={styles.box2}>
                        <Text style={styles.text}>Header</Text>
                    </View>
                    <View style={styles.box1}>
                        <Text style={styles.text}>X</Text>
                    </View>
                </View>
                <View style={styles.content}>
                    <Text style={styles.text}>ScrollView</Text>
                </View>
                <View style={styles.header}>
                    <View style={styles.box1}>
                        <Text style={styles.text}>I</Text>
                    </View>
                    <View style={styles.box1}>
                        <Text style={styles.text}>C</Text>
                    </View>
                    <View style={styles.box1}>
                        <Text style={styles.text}>O</Text>
                    </View>
                    <View style={styles.box1}>
                        <Text style={styles.text}>N</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#5DADE2",
        flex: 1
    },

    header: {
        backgroundColor: "white",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: -8
    },

    headerText: {
        color: "black",
        fontSize: 20,
        fontWeight: "bold",
    },

    content: {
        backgroundColor: "blue",
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },

    box1: {
        backgroundColor: "green",
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        backgroundColor: "green",
        padding: 10,
        margin: 2,
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },

    row: {
        backgroundColor: "white",
        padding: 5,
        margin: 8,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center'
    },

    column: {
        flexDirection: "column"
    },

    text: {
        color: "white",
        fontSize: 20
    },
});
export default App;